(in-package :functional-enumerators)

(defun make-concatenated-name (package-example &rest parts)
  (intern
    (apply 'concatenate 'string
           (loop
             for p in parts
             collect
             (cond
               ((stringp p) p)
               ((symbolp p) (symbol-name p))
               (t (format nil "~a" p)))))
    (cond
      ((null package-example) *package*)
      ((symbolp package-example) (symbol-package package-example))
      ((stringp package-example) (find-package package-example))
      ((packagep package-example) package-example)
      (t (error "Unexpected package example: ~a" package-example)))))
