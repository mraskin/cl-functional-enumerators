(in-package :functional-enumerators)

(def-functional-enumerator
  concatenation ((local-state rest enumerator) ())
  (
   :documentation "A concatenation of functional enumerators"
   )
  (initial (e &rest args)
           (loop
             for es on args
             for current-e := (first es)
             for rest := (rest es)
             for local-state := (initial current-e)
             when local-state
             return (make-state local-state rest e)))
  (value (s) (value local-state))
  (next (s)
        (let ((local (next local-state)))
          (if local
            (make-state local rest enumerator)
            (initial (cons enumerator rest))))))

(def-functional-enumerator
  map ((function states) (function &rest enums))
  (
   :package :functional-enumerators
   :documentation "An application of a given function to arguments from an enumerator"
   )
  (initial (e &rest args)
           (let
             ((states
                (loop for e in enums
                      for s := (initial e)
                      unless s return nil
                      collect s)))
             (when states (make-state function states))))
  (value (s)
         (apply function
                (loop for s in states
                      collect (value s))))
  (next (s)
        (let
          ((new-states
             (loop
               for s in states
               for ns := (next s)
               unless ns return nil
               collect ns)))
          (when new-states (make-state function new-states)))))

(def-functional-enumerator
  filter ((predicate states) (predicate enum &rest enums))
  (
   :helpers (collect-values all-next skip-bad)
   :documentation "An enumerator of elements satisfying the predicate"
   )
  (collect-values (states)
                  (loop for s in states
                        collect (value s)))
  (all-next (states)
            (loop for s in states
                  for ns := (next s)
                  unless ns return nil
                  collect ns))
  (skip-bad (states predicate)
            (loop
              for ss := states then (all-next ss)
              while ss
              when (apply predicate (collect-values ss))
              return ss))
  (initial (e &rest args)
           (let
             ((states
                (skip-bad
                  (loop
                    for e in (cons enum enums)
                    for s := (initial e)
                    unless s return nil
                    collect s)
                  predicate)))
             (make-state predicate states)))
  (next (s) (let ((new-states (skip-bad (all-next states) predicate)))
                (when new-states
                  (make-state predicate new-states))))
  (value (s) (value (first states))))

