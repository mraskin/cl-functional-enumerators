;;;; LICENSE: see LICENSE.text
;;;; AUTHOR: Michael Raskin

;;; ASDF system definition for Functional-Enumerators

(in-package :asdf-user)

(defsystem
  :functional-enumerators
  :description "Functional enumerators: immutable-value-based object enumeration"
  :name "functional-enumerators"
  :version "0.0"
  :author "Michael Raskin"
  :serial nil
  :components
  (
   (:file "package")
   (:file "util" :depends-on ("package"))
   (:file "core-type" :depends-on ("package" "util"))
   (:file "basic-constructors" :depends-on ("core-type"))
   (:file "basic-transformations" :depends-on ("core-type"))
   (:file "basic-functions" :depends-on ("core-type"))

   (:file "test" :depends-on ("package" "core-type" "basic-constructors" "basic-transformations" "basic-functions"))
   ))

