(in-package :functional-enumerators)

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-list* '(1 2 3 4))
    '(1 2 3 4)))

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-range* 1 4)
    '(1 2 3 4)))

(assert
  (equal
    (enumerator-to-list
      (list *functional-enumerator-range* 1 4))
    '(1 2 3 4)))

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-subsets* '(1 2 3))
    '(() (1) (2) (1 2) (3) (1 3) (2 3) (1 2 3))))

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-concatenation*
      (list *functional-enumerator-list* '(1 2 3))
      (list *functional-enumerator-range* 4 6))
    '(1 2 3 4 5 6)))

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-map*
      '*
      (list *functional-enumerator-list* '(1 2 3))
      (list *functional-enumerator-range* 2 5))
    '(2 6 12)))

(assert
  (equal
    (enumerator-to-list
      *functional-enumerator-filter*
      '<
      (list *functional-enumerator-list* '(1 3 5 7 9))
      (list *functional-enumerator-list* '(1 2 4 8 16 32)))
    '(7 9)))

(assert
  (equal
    (enumerator-to-list
      (make-instance 'functional-enumerator-concatenation)
      (list (make-instance 'functional-enumerator-list) '(1 2 3))
      (list (make-instance 'functional-enumerator-range) 4 6))
    '(1 2 3 4 5 6)))

(def-functional-enumerator
  test-squares ((current min max) (min max)) ()
  (initial (e &rest args)
           (when (<= min max) (make-state min min max)))
  (value (s) (* current current))
  (next (s) (when (< current max) (make-state (1+ current) min max))))

(def-functional-enumerator
  test-squares-brief ((current min max) (min max)) ()
  (initial ()
           (when (<= min max) (make-state min min max)))
  (value () (* current current))
  (next () (when (< current max) (make-state (1+ current) min max))))

(assert
  (equal
    (enumerator-to-list
      (list *functional-enumerator-test-squares* 1 2))
    '(1 4)))

(assert
  (equal
    (enumerator-to-list
      (make-instance 'functional-enumerator-test-squares)
      1 2)
    '(1 4)))

(with-functional-enumerator-abbreviations
  ()
  (let*
    ((e *functional-enumerator-test-squares*)
     (s (initial e 1 2))
     (ns (next s))
     (ns-alt (enumerator-next-state s))
     (v (value s))
     (nv (value ns))
     (nns (next ns)))
    (assert (= v 1))
    (assert (= nv 4))
    (assert (null nns))
    (assert
      (equal
        (format nil "~a" ns)
        (format nil "~a" ns-alt)))
    (assert 
      (equal
        (format nil "~a" s)
        (format nil "#<~s CURRENT: 1 MIN: 1 MAX: 2>"
                'functional-enumeration-state-test-squares)))))
