(in-package :functional-enumerators)

(defgeneric enumerator-to-list (enumerator &rest args)
            (:documentation "Collect all the values returned by enumerator into a list"))

(defmethod enumerator-to-list (enumerator &rest args)
  (with-functional-enumerator-abbreviations
    ()
    (loop
      for state := (initial (cons enumerator args)) then (next state)
      while state collect (value state))))

