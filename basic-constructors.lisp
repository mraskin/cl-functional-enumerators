(in-package :functional-enumerators)

(def-functional-enumerator
  list ((data) (data))
  (
   :package :functional-enumerators
   :documentation "A functional enumerator that enumerates a given list"
   )
  (initial (e &rest args) (make-state data))
  (next (s) (when (cdr data) (make-state (cdr data))))
  (value (s) (car data)))

(def-functional-enumerator
  range ((current low high) (low high))
  (:documentation "A functional enumerator that enumerates a range of integers")
  (initial (e &rest args)
         (when (<= low high) (make-state low low high)))
  (next (s)
        (when (< current high)
          (make-state (1+ current) low high)))
  (value (s) current))

(def-functional-enumerator
  subsets ((data inclusion) (data))
  (:documentation "A functional enumerator that enumerates all subsets of a set")
  (value (s)
         (loop
           for element in data
           for bit in inclusion
           when bit collect element))
  (initial (e &rest args)
           (make-state data (make-list (length data))))
  (next (s)
        (loop
          for tail on inclusion
          for k upfrom 0
          unless (first tail)
          return (make-state
                   data
                   (append
                     (make-list k)
                     (list t)
                     (rest tail))))))

