(in-package :functional-enumerators)

(defgeneric enumerator-initial-state (enumerator &rest args)
            (:documentation "Compute the first enumeration state"))
(defgeneric enumerator-next-state (state)
            (:documentation "Compute the next state"))
(defgeneric enumerator-state-value (state)
            (:documentation "Compute the value corresponding to a state"))

(defgeneric enumerator-previous-state (state)
            (:documentation "Compute the previous state"))
(defgeneric enumerator-clone-state (state)
            (:documentation "Copy a state"))

(defgeneric enumerator-increment-state (state)
            (:documentation "Increment a state"))
(defgeneric enumerator-decrement-state (state)
            (:documentation "Decrement a state"))

(defgeneric enumerator-nth-state (state n)
            (:documentation "Compute a state by its offset from the initial one"))
(defgeneric enumerator-nth-value (state n)
            (:documentation "Compute a value by its offset from the initial state"))

(defgeneric enumerator-initial-state-thunk (enumerator)
            (:documentation "Retrieve a function that returns the initial enumeration state"))
(defgeneric enumerator-next-state-function (enumerator)
            (:documentation "Retrieve a function that computes the next state"))
(defgeneric enumerator-state-value-function (enumerator)
            (:documentation "Retrieve a function that computes the value corresponding to a state"))

(defgeneric enumerator-previous-state-function (enumerator)
            (:documentation "Retrieve a function that computes the previous state"))
(defgeneric enumerator-clone-state-function (enumerator)
            (:documentation "Retrieve a function that copies a state"))

(defgeneric enumerator-increment-state-function (enumerator)
            (:documentation "Retrieve a function that increments a state"))
(defgeneric enumerator-decrement-state-function (enumerator)
            (:documentation "Retrieve a function that decrements a state"))

(defgeneric enumerator-nth-state-function (enumerator)
            (:documentation "Retrieve a function that returns a state by its offset from the initial one"))
(defgeneric enumerator-nth-value-function (enumerator)
            (:documentation "Retrieve a function that returns a value by its offset from the initial state"))

; We allow to pack enumerator and initialisation data into a list
(defmethod enumerator-initial-state ((enumerator list) &rest args)
  (let
    ((real-enumerator (first enumerator)))
    (assert real-enumerator)
    (values
      (apply 'enumerator-initial-state (append enumerator args))
      real-enumerator)))

(defclass functional-enumerator () ()
  (:documentation
    "A mixin to assert support for the basic functional enumerator interface"))

(defclass functional-enumeration-state () ()
  (:documentation
    "A mixin to assert being a state of some functional enumerator"))

(defclass functional-two-way-enumerator () ()
  (:documentation
    "A mixin to assert support for the two-way functional enumerator interface"))

(defclass functional-destructive-enumerator () ()
  (:documentation
    "A mixin to assert support for the optimized destructive functional enumerator interface"))

(defun enumerator-abbreviation-list (package-example)
  `(
    (,(make-concatenated-name package-example 'initial)         . enumerator-initial-state   )
    (,(make-concatenated-name package-example 'next)            . enumerator-next-state      )
    (,(make-concatenated-name package-example 'value)           . enumerator-state-value     )
    (,(make-concatenated-name package-example 'initial-state)   . enumerator-initial-state   )
    (,(make-concatenated-name package-example 'next-state)      . enumerator-next-state      )
    (,(make-concatenated-name package-example 'state-value)     . enumerator-state-value     )

    (,(make-concatenated-name package-example 'previous-state)  . enumerator-previous-state  )
    (,(make-concatenated-name package-example 'previous)        . enumerator-previous-state  )
    (,(make-concatenated-name package-example 'prev-state)      . enumerator-previous-state  )
    (,(make-concatenated-name package-example 'prev)            . enumerator-previous-state  )

    (,(make-concatenated-name package-example 'clone-state)     . enumerator-clone-state     )
    (,(make-concatenated-name package-example 'clone)           . enumerator-clone-state     )
    (,(make-concatenated-name package-example 'increment-state) . enumerator-increment-state )
    (,(make-concatenated-name package-example 'inc-state)       . enumerator-increment-state )
    (,(make-concatenated-name package-example 'decrement-state) . enumerator-decrement-state )
    (,(make-concatenated-name package-example 'dec-state)       . enumerator-decrement-state )

    (,(make-concatenated-name package-example 'indexed-state)   . enumerator-nth-state       )
    (,(make-concatenated-name package-example 'indexed-value)   . enumerator-nth-value       )
    ))

(defmacro with-functional-enumerator-abbreviations ((&optional package-example) &rest body)
  "Execute a piece of code in an environment with local macros reducing the verbosity of functional enumerator operations"
  `(macrolet
     ,(loop
        for abbr in (enumerator-abbreviation-list package-example)
        collect
        `(,(car abbr) (&rest args) (cons ',(cdr abbr) args)))
     ,@body))

(defmacro def-functional-enumerator
  (name (state-parts initialisation-data-parts)
        (&key
          extra-superclasses extra-state-superclasses
          documentation package helpers (create-instance t)
          enumerator-slots)
        &rest definitions)
  (let
    ((package (when package (if (packagep package) package (find-package package)))))
    (labels
      ((make-slot (slot-definition)
                  (if (listp slot-definition)
                    slot-definition
                    `(,slot-definition
                       :accessor
                       ,(make-concatenated-name 
                          (or package name)
                          '#:functional-enumerator- name 
                          '#:- slot-definition)
                       :initarg 
                       ,(make-concatenated-name :keyword slot-definition))))
       (slot-name (slot-definition)
                  (if (listp slot-definition) (first slot-definition) slot-definition))
       (slot-keyword (slot-definition)
                     (make-concatenated-name :keyword (slot-name slot-definition))))
      (let*
        (
         (class-name (make-concatenated-name (or package name)
                                             '#:functional-enumerator- name))
         (state-class-name
           (make-concatenated-name (or package name)
                                   '#:functional-enumeration-state- name))
         (abbreviations (enumerator-abbreviation-list (or package name)))
         )
        `(progn
           (defclass ,class-name (functional-enumerator ,@extra-superclasses)
             ,(mapcar #'make-slot enumerator-slots)
             ,@(when documentation `((:documentation ,documentation))))
           (defmethod print-object ((object ,class-name) stream)
             (format stream "#<~s~{ ~a: ~s~}>"
                     ',class-name
                     (list
                       ,@(loop for slot in enumerator-slots
                               collect (symbol-name (slot-name slot))
                               collect `(slot-value object ',(slot-name slot))))))
           (defclass ,state-class-name
             (functional-enumeration-state ,@extra-state-superclasses)
             ,(mapcar #'make-slot state-parts)
             (:documentation ,(format nil "Enumeration state for ~a" class-name)))
           (defmethod print-object ((object ,state-class-name) stream)
             (format stream "#<~s~{ ~a: ~s~}>"
                     ',state-class-name
                     (list
                       ,@(loop for slot in state-parts
                               collect (symbol-name (slot-name slot))
                               collect `(slot-value object ',(slot-name slot))))))
           ,(let
              (method-definitions helper-definitions)
              (loop
                for d in definitions
                for definition-name := (first d)
                for helperp := (find definition-name helpers)
                for full-name :=
                (if helperp definition-name
                  (cdr (assoc definition-name abbreviations)))
                for body := (rest (rest d))
                for has-enumerator-parameter :=
                (and
                  (eq full-name 'enumerator-initial-state)
                  (not helperp))
                for arglist-passed := (second d)
                for arglist :=
                (cond
                  (arglist-passed arglist-passed)
                  (helperp ())
                  ((eq full-name 'enumerator-initial-state)
                   (list (gensym "FUNCTIONAL-ENUMERATION-ENUMERATOR")
                         '&rest (gensym "FUNCTIONAL-ENUMERATION-ARGS")))
                  (t (list (gensym "FUNCTIONAL-ENUMERATION-STATE"))))
                for enumerator-name :=
                (when has-enumerator-parameter (first arglist))
                for state-name :=
                (when (and (not has-enumerator-parameter)
                           (not helperp))
                  (first arglist))
                for initialization-data-name :=
                (when (and (eq full-name 'enumerator-initial-state)
                           (not helperp))
                  (assert (equal (second arglist) '&rest))
                  (third arglist))
                for body-wrapped :=
                (let*
                  ((body body)
                   (body
                     (if (or initialization-data-name
                             helperp)
                       body
                       `((with-slots ,(mapcar #'slot-name state-parts)
                           ,state-name
                           ,@body))))
                   (body
                     (if (and
                           initialization-data-name
                           initialisation-data-parts)
                       `((destructuring-bind
                           ,(mapcar #'slot-name initialisation-data-parts)
                           ,initialization-data-name
                           ,@body))
                       body))
                   (body
                     (if (and enumerator-name enumerator-slots)
                       `((with-slots ,(mapcar #'slot-name enumerator-slots)
                           ,enumerator-name
                           ,@body
                           ))
                       body)))
                  body)
                for expanded-arglist :=
                (if helperp arglist
                  `(,@(when enumerator-name
                        `((,enumerator-name ,class-name)))
                    ,@(cond
                        (initialization-data-name
                          `(&rest ,initialization-data-name))
                        (t
                          `((,state-name ,state-class-name))))))
                for code :=
                `(,@(unless helperp `(defmethod))
                   ,full-name
                   ,expanded-arglist
                   (with-functional-enumerator-abbreviations
                     (,(or package name))
                     (macrolet
                       ((,(make-concatenated-name (or package name) 'make-state-instance)
                          ,(mapcar #'slot-name state-parts)
                          `(make-instance 
                             ',',state-class-name
                             ,,@(loop
                                  for slot in state-parts
                                  collect (slot-keyword slot)
                                  collect (slot-name slot))))
                        (,(make-concatenated-name (or package name) 'make-state)
                          (&rest args) `(make-state-instance ,@args))
                        (,(make-concatenated-name (or package name) 'state)
                          (&rest args) `(make-state-instance ,@args)))
                       ,@body-wrapped)))
                do (assert full-name)
                do (if helperp (push code helper-definitions)
                     (push code method-definitions)))
              `(labels
                 ,(reverse helper-definitions)
                 ,@(reverse method-definitions)))
           ,@(when create-instance
               `((defparameter ,(make-concatenated-name class-name '#:* class-name '#:*)
                   (make-instance ',class-name)))))))))

; We assume that the final state is always returned in the canonical form ⊥
; We assume that every operation on ⊥ returns ⊥
;
; The basic operations are:
; 
; first: function calculating the first state; may fail to terminate
; next: function calculating the transition; may fail to terminate
; value: calculate the value corresponding to the state; must terminate
; 
; 
; Concatenation:
; 
; first(E∘E') is
;   <b=0, q=first(E)> or <b=1, q=first(E')>
; 
; next(E∘E', q) is
;   if q.b=0 then
;     <b=0, q=next(E, q.q)> or <b=1, q=first(E')>
;   else
;     <b=1, q=next(E', q.q)>
; 
; value(E∘E', q) is
;   if q.b=0 then value(E, q.q) else value(E', q.q)
;
;
; Parallel enumeration:
; 
; first(<E, E'>) is <q=first(E), q'=first(E')>
;
; next(<E, E'>, q) is <q=next(E,q.q), q'=next(E', q.q')>
;
; value(<E, E'>, q) is <value(E, q.q), value(E', q.q')>
;
;
; Filtering:
;
; let find-if(E, q, p) be
;   let qc := q
;     while qc≠⊥ ∧ ¬p(value(E,qc))
;       qc := next(E, qc)
;     return qc
; [with standard ⊥ shortcut]
;
; first(filter(E, p)) is find-if(E, first(E), p)
;
; next(filter(E,p), q) is find-if(E, next(E, q), p)
;
; value(filter(E,p), q) is value(E, q)
;
;
; Removing duplicates:
;
; first(remove-duplicates(E)) is <q=first(E), l={}>
;
; value(remove-duplicates(E), q) is value(E, q.q)
;
; next(remove-duplicates(E), q) is
;   <q=find-if(E, next(E, q), x ↦ x∉q.l),
;    l=q.l ∪ {value(E, q.q)}>
;
;
; Flattening a single level:
; 
; first(flatten(E)) is
;   let q := first(E)
;   while (q ≠ ⊥) ∧ (q' := first(value(E, q))) = ⊥
;     q := next(E, q)
;   return <q=q, q'=q'>
;
; next(flatten(E), q) is
;   let q' := next(value(q.q), q.q')
;     if q'≠⊥ then <q=q.q, q'=q'> else
;       let q := next(E, q.q)
;       while (q ≠ ⊥) ∧ (q' := first(value(E, q))) = ⊥
;         q := next(E, q)
;       return <q=q, q'=q'>
;
; value(flatten(E), q) is
;   value(value(q.q), q.q')
